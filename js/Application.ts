// I don't know what the param names should be, so they
// need to be changed to sensible names
declare class Angular
{
	module (param1: string, param2: any[]) : Angular;
	controller(name: string, controllerConstructor: Function): Angular;
	controller(name: string, inlineAnnotadedConstructor: any[]): Angular;
	service(name: string, serviceConstructor: Function): Angular;
	service(name: string, inlineAnnotadedConstructor: any[]): Angular;
	config(param1: any[]) : Angular;
}


declare var angular: Angular;


angular
	.module('phonebook', [])
		.controller('contactsController', Phonebook.Controllers.Contacts.prototype.injection())
		.service('contactsService', Phonebook.Storages.LocalContacts.prototype.injection())
;



/**
 * @type {angular.Module}
 * /
module todos {
	'use strict';

	angular.module('phonebook', [])
//	var phonebook = angular.module('phonebook', [])
//			.controller('todoCtrl', Phonebook.PhonebookController.prototype.injection())
//			.directive('todoBlur', TodoBlur.prototype.injection())
//			.directive('todoFocus', TodoFocus.prototype.injection())
//			.service('todoStorage', TodoStorage.prototype.injection())
//			;
}
*/
