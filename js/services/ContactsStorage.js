var Phonebook;
(function (Phonebook) {
    (function (Storages) {
        'use strict';

        var LocalContacts = (function () {
            function LocalContacts() {
                /**
                * Jméno klíče v uložišti.
                */
                this.STORAGE_ID = 'phonebook-contacts';
                /**
                * Nakešovaný seznam.
                */
                this.content = [];
                this.content = JSON.parse(localStorage.getItem(this.STORAGE_ID) || '[]');
            }
            LocalContacts.prototype.injection = function () {
                return [
                    LocalContacts
                ];
            };

            /**
            * Přidání nového záznamu.
            */
            LocalContacts.prototype.push = function (entry) {
                console.log('CMS:push', entry);
                this.content.push(entry);
                localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
            };

            /**
            * Přidání nového záznamu.
            */
            LocalContacts.prototype.update = function (entry) {
                console.log('CMS:update', entry);
                localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
            };

            /**
            * Odstranění záznamu.
            */
            LocalContacts.prototype.remove = function (entry) {
                console.log('CMS:remove', entry);
                var index = this.content.indexOf(entry);
                if (index >= 0) {
                    this.content.splice(index, 1);
                }
                localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
            };

            /**
            * Získání seznamu záznamů.
            */
            LocalContacts.prototype.find = function () {
                console.log('storage:find');
                return this.content;
            };
            return LocalContacts;
        })();
        Storages.LocalContacts = LocalContacts;
    })(Phonebook.Storages || (Phonebook.Storages = {}));
    var Storages = Phonebook.Storages;
})(Phonebook || (Phonebook = {}));
