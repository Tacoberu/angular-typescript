module Phonebook.Storages
{
	'use strict';


	export interface IContacts
	{
		push(entry: Phonebook.Models.Contact): void;
		update(entry: Phonebook.Models.Contact): void;
		remove(entry: Phonebook.Models.Contact): void;
		find() : Phonebook.Models.Contact[];
	}



	export class LocalContacts implements IContacts
	{

		public injection(): any[]
		{
			return [
				LocalContacts
			]
		}


		/**
		 * Jméno klíče v uložišti.
		 */
		STORAGE_ID = 'phonebook-contacts';


		/**
		 * Nakešovaný seznam.
		 */
		private content = []


		constructor()
		{
			this.content = JSON.parse(localStorage.getItem(this.STORAGE_ID) || '[]');
		}



		/**
		 * Přidání nového záznamu.
		 */
		public push(entry: Phonebook.Models.Contact) : void
		{
			console.log('CMS:push', entry);
			this.content.push(entry);
			localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
		}



		/**
		 * Přidání nového záznamu.
		 */
		public update(entry: Phonebook.Models.Contact) : void
		{
			console.log('CMS:update', entry);
			localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
		}



		/**
		 * Odstranění záznamu.
		 */
		public remove(entry: Phonebook.Models.Contact) : void
		{
			console.log('CMS:remove', entry);
			var index = this.content.indexOf(entry);
			if (index >= 0) {
				this.content.splice(index, 1);
			}
			localStorage.setItem(this.STORAGE_ID, JSON.stringify(this.content));
		}


		/**
		 * Získání seznamu záznamů.
		 */
		public find() : Phonebook.Models.Contact[]
		{
			console.log('storage:find');
			return this.content;
		}

	}


}
