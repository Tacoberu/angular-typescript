var Phonebook;
(function (Phonebook) {
    (function (Controllers) {
        var Contacts = (function () {
            function Contacts($scope, contactsService) {
                console.log('A', contactsService);

                var contacts = new Phonebook.Models.Contacts(contactsService);
                $scope.editing = false;

                /**
                * Ctení záznamů.
                */
                $scope.read = function () {
                    console.log("read");
                    return contacts.find();
                };

                /**
                * Přidání nového záznamu,
                */
                $scope.store = function () {
                    console.log('store', $scope.form);
                    if ($scope.editing == true) {
                        contacts.update($scope.form);
                    } else {
                        contacts.push($scope.form);
                    }

                    $scope.form = null;
                };

                /**
                * Přidání nového záznamu,
                */
                $scope.edit = function (entry) {
                    console.log('edit', entry);
                    $scope.form = entry;
                    $scope.editing = true;
                };

                /**
                * Odstranění záznamu,
                */
                $scope.delete = function (entry) {
                    console.log("delete", entry);
                    contacts.remove(entry);
                };
            }
            /**
            * this method is called on prototype during registration into IoC container.
            * It provides $injector with information about dependencies to be injected into constructor
            * it is better to have it close to the constructor, because the parameters must match in count and type.
            */
            Contacts.prototype.injection = function () {
                return [
                    '$scope',
                    'contactsService',
                    Contacts
                ];
            };
            return Contacts;
        })();
        Controllers.Contacts = Contacts;
    })(Phonebook.Controllers || (Phonebook.Controllers = {}));
    var Controllers = Phonebook.Controllers;
})(Phonebook || (Phonebook = {}));
