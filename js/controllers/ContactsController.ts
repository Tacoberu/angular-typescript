module Phonebook.Controllers
{
	export interface Scope
	{
		editing: boolean;
		form: Phonebook.Models.Contact;
		store: Function;
		edit: Function;
		delete: Function;
		read: Function;
	}


	export class Contacts
	{

		/**
		 * this method is called on prototype during registration into IoC container.
		 * It provides $injector with information about dependencies to be injected into constructor
		 * it is better to have it close to the constructor, because the parameters must match in count and type.
		 */
		public injection(): any[]
		{
			return [
				'$scope',
				'contactsService',
				Contacts
			]
		}

		constructor ($scope: Scope, contactsService: Phonebook.Storages.IContacts)
		{
			console.log('A', contactsService);

			var contacts = new Phonebook.Models.Contacts(contactsService);
			$scope.editing = false;


			/**
			 * Ctení záznamů.
			 */
			$scope.read = function()
			{
				console.log("read")
				return contacts.find();
			}


			/**
			 * Přidání nového záznamu,
			 */
			$scope.store = function ()
			{
				console.log('store', $scope.form);
				if ($scope.editing == true) {
					contacts.update($scope.form);
				}
				else {
					contacts.push($scope.form);
				}

				$scope.form = null;
			};



			/**
			 * Přidání nového záznamu,
			 */
			$scope.edit = function (entry: Phonebook.Models.Contact)
			{
				console.log('edit', entry);
				$scope.form = entry;
				$scope.editing = true;
			};



			/**
			 * Odstranění záznamu,
			 */
			$scope.delete = function (entry: Phonebook.Models.Contact)
			{
				console.log("delete", entry)
				contacts.remove(entry);
			}

		}
	}
}
