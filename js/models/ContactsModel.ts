module Phonebook.Models
{
	'use strict';


	/**
	 * Instance jednoho kontaktu.
	 */
	export class Contact
	{
		/**
		 * @param name Jméno klienta.
		 * @param phone Telefonní kontakt.
		 * @param link Odkaz na web
		 * @param age Věk
		 */
		constructor(
			public name: string,
			public phone: string,
			public link: string,
			public age: number
			)
		{}

	}



	/**
	 * Instance jednoho kontaktu.
	 */
	export class Contacts
	{

		public injection(): any[]
		{
			return [
				Contacts
			]
		}



		/**
		 * @param storage Uložiště.
		 */
		constructor(private storage:Phonebook.Storages.IContacts)
		{}


		/**
		 * Přidání nového záznamu.
		 */
		public push(entry: Contact) : void
		{
			this.storage.push(entry);
		}


		/**
		 * Přidání nového záznamu.
		 */
		public update(entry: Contact) : void
		{
			this.storage.update(entry);
		}


		/**
		 * Přidání nového záznamu.
		 */
		public remove(entry: Contact) : void
		{
			this.storage.remove(entry);
		}


		/**
		 * Získání seznamu záznamů.
		 */
		public find() : Contact[]
		{
			console.log("model.find")
			return this.storage.find();
		}


	}


}
