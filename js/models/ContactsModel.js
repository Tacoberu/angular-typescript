var Phonebook;
(function (Phonebook) {
    (function (Models) {
        'use strict';

        /**
        * Instance jednoho kontaktu.
        */
        var Contact = (function () {
            /**
            * @param name Jméno klienta.
            * @param phone Telefonní kontakt.
            * @param link Odkaz na web
            * @param age Věk
            */
            function Contact(name, phone, link, age) {
                this.name = name;
                this.phone = phone;
                this.link = link;
                this.age = age;
            }
            return Contact;
        })();
        Models.Contact = Contact;

        /**
        * Instance jednoho kontaktu.
        */
        var Contacts = (function () {
            /**
            * @param storage Uložiště.
            */
            function Contacts(storage) {
                this.storage = storage;
            }
            Contacts.prototype.injection = function () {
                return [
                    Contacts
                ];
            };

            /**
            * Přidání nového záznamu.
            */
            Contacts.prototype.push = function (entry) {
                this.storage.push(entry);
            };

            /**
            * Přidání nového záznamu.
            */
            Contacts.prototype.update = function (entry) {
                this.storage.update(entry);
            };

            /**
            * Přidání nového záznamu.
            */
            Contacts.prototype.remove = function (entry) {
                this.storage.remove(entry);
            };

            /**
            * Získání seznamu záznamů.
            */
            Contacts.prototype.find = function () {
                console.log("model.find");
                return this.storage.find();
            };
            return Contacts;
        })();
        Models.Contacts = Contacts;
    })(Phonebook.Models || (Phonebook.Models = {}));
    var Models = Phonebook.Models;
})(Phonebook || (Phonebook = {}));
